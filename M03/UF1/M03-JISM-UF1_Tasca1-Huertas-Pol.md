#INSTAL·LACIÓ DE MOODLE AMB DOCKER-COMPOSE
**Prerequisit:** Tenir instal·lat Docker.

Docker us ve preinstal·lat al Debian 11 de l’escola. Si ho volguéssiu instal·lar a casa, primerhaurieu d’instal·lar Docker.

El següent que farem és instal·lar Docker-compose.Primer descarregarem la última versió estable de docker-compose. *Escriurem al terminal:*
```sh
$ sudo curl -L"https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
Donarem permisos al docker-compose:
```sh
$ sudo chmod +x /usr/local/bin/docker-compose
```
Comprovarem que hem instal·lat correctament docker-compose:
```sh
$ sudo docker-compose --version
```
Descarreguem el fitxerdocker-compose.ymlde moodle.El guardem a un directori quetinguem permisos.

Un altre cop al terminal i dins del directori on tinguem descarregat el docker-compose.yml,escriurem:
```sh
$ sudo docker-compose up -d
```
Se’ns obrirà un moodle. Podem logar-nos fent servir les següentscredencials:

- usuari: user

- contrassenya: bitnam

[link moodle](https://moodle.escoladeltreball.org/course/view.php?id=2129)

![Maincra](1635847206236.jpg)

![meme de thanos](/home/users/inf/jism1/ism47997052/Documents/images.jpg)

![Mentalidad de Tiburon](fotos/mentalidad de tiburon.jpg)

[![Metaverso](1635847206236.jpg)](https://www.elmundo.es/economia/empresas/2021/10/28/617abe08e4d4d83c6e8b45a1.html)

Quan s’implanta un [paquet ofimàtic][1] (o qualsevol altre programari) poden sortirproblemes, que haureu de resoldre i documentar. Per documentar-los, explicareuel problema, les possibles causes i les solucions aplicades. Aquests problemespoden ser de natura molt diferent:

•Problemes amb el maquinari. El [programari][2] que instal·leu pot tenir unsrequisits de programari que no compleix algun ordinador, o bé que, tot icomplir-los, són tan justos que fan que el treball amb aquesta aplicació siguipoc efectiu. Caldrà prendre nota d’aquestes circumstàncies, i millorar en lamesura del possible els ordinadors que presenten aquests problemes.


•Problemes amb el [programari][2] existent o el sistema operatiu. Si durantla instal·lació falta algun component clau del sistema operatiu, o algunprogramari que ja hi ha entra en conflicte amb el que intenteu instal·lar.

[1]:<https://www.markdownguide.org/basic-syntax/>
[2]:<https://es.wikipedia.org/wiki/Software>


| Hola | Adios|
| ------ | ------ |
| Hola | Adios |
| Hola | Adios |
| Hola | Adios |
| Hola | Adios |
| Hola| Adios |


- [ ] Mercury
- [x] Venus
- [x] Earth (Orbit/Moon)
- [x] Mars
- [ ] Jupiter
- [ ] Saturn
- [ ] Uranus
- [ ] Neptune
- [ ] Comet Haley

:bowtie:
	
:smile:
